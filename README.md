Exercices JS du 20190207
========================

Simplon Chambéry Promo1

# Exercice 1

Un clique sur le bouton affiche une boîte de dialogue "Bonjour"

# Exercice 2

Un clique sur le bouton affiche "Je suis plein" à la place de "Je suis vide"

# Exercice 3

À chaque clique, on indique combien de fois l'utilisateur-trice à cliquer sur le bouton.

# Exercice 4

Cliquer pour mettre l'eau dans le verre

# Exercice 5

A chaque clique on ajoute une rasade d'eau au-dessus de la précédente, sans que le verre ne déborde.
